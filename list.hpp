///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file list.hpp
/// @version 1.0
///
/// list functions
///
/// @author Brooke Maeda <bmhm@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   5 April 2021
///////////////////////////////////////////////////////////////////////////////


#pragma once
#include "node.hpp"

using namespace std;

class DoubleLinkedList{
protected:
   Node* head = nullptr;
   unsigned int numOfNodes = 0;
   Node* tail = nullptr;

public:
   const bool empty() const;
   void push_front( Node* newNode);
   Node* pop_front();
   Node* get_first() const;
   Node* get_next( const Node* currentNode ) const;
   inline unsigned int size() const { return numOfNodes; };
   
   void push_back( Node* newNode);
   Node* pop_back();
   Node* get_last() const;
   Node* get_prev( const Node* currentNode ) const;

   void swap( Node* node1, Node* node2);
   
   const bool isSorted() const; //this function depends on Node's < operator
   void insertionSort(); //this function depends on Node's < operator
   
   bool validate() const;

   void insert_after( Node* currentNode, Node* newNode );
   void insert_before( Node* currentNode, Node* newNode);
   const bool isIn( Node* aNode ) const;
};


