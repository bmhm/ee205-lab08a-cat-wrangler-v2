///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file the.cpp
/// @version 1.0
///
/// test
///
/// @author Brooke Maeda <bmhm@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   1 April 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <cassert>

#include "cat.hpp"
#include "list.hpp"

using namespace std;

int main(){
   cout << "Hello World" << endl;

   Node node; //Instantiate a node
   DoubleLinkedList list; //Instantiate a DoubleLinkedList
   
   cout << "is empty? " << boolalpha << list.empty() << endl;
   //testing push_front
   list.push_front(new Node() );
   cout << "added new node" << endl;
   cout << "is empty?" << boolalpha << list.empty() << endl;
   list.push_front(new Node() );
   cout << "added another node" << endl;
   //testing size
   cout << "size: " << list.size() << endl;
   //testing pop front
   list.pop_front();
   cout << "removed node" << endl;
   cout << "size: " << list.size() << endl;
   //testing get first
   cout << "first node: " << list.get_first() << endl;

   list.push_front(new Node() );
   list.push_front(new Node() );
   list.push_front(new Node() );
   cout << "added more nodes, new size: " << list.size() << endl;
   
   //testing push_back
   list.push_back(new Node() );
   cout << "added new node to back " << endl;
   cout << "size: " << list.size() << endl;


   //testing pop_back
   list.pop_back();
   cout << "removed node from back" << endl;
   cout << "size: " << list.size() << endl;
  
   //testing get last
   cout << "last node: " << list.get_last() << endl;

}



