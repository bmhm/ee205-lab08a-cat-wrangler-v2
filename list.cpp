///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file list.cpp
/// @version 1.0
///
/// list functions
///
/// @author Brooke Maeda <bmhm@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   5 April 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include "list.hpp"
#include <cassert>

using namespace std;


//return true if list is empty, false is anything in list
const bool DoubleLinkedList::empty() const{
   return head == nullptr && tail == nullptr;

}
   

//add newNode to the front of the list
void DoubleLinkedList::push_front( Node* newNode){
   if( newNode == nullptr)
      return;//if new node thing then just return
   if( head!= nullptr ){//if list not empty
      newNode->next = head;
      newNode->prev = nullptr;
      head->prev = newNode;
      head = newNode;
   }
   else{//if list empty
      newNode->next = nullptr;
      newNode->prev = nullptr;
      head = newNode;
      tail = newNode;
   }
   numOfNodes++;

}

//remove a node from the front, if list empty return nullptr
Node* DoubleLinkedList::pop_front(){
   if( head == nullptr )
      return nullptr;//if empty then return nullptr
   
   if( head == tail ) { //if list only has one
      Node* returning = head;
      head = nullptr;
      tail = nullptr;
      numOfNodes--;
      return returning;
   }
   else{
      Node* returning = head;
      head = head->next;
      head->prev = nullptr;
      returning->next = nullptr;
      numOfNodes--;
      return returning;
   }
   

}
   
//return the very first node from the list, dont make any changes to list
Node* DoubleLinkedList::get_first() const{
   return head;

}
   

//return the node immediately following currentNode
Node* DoubleLinkedList::get_next( const Node* currentNode ) const{
   if( head == nullptr )
      return nullptr; //if list empty
  
   return currentNode->next;

}
   

//return the number of nodes in the list
//size is now in header file since its inline


//add newNode to the back of the list
void DoubleLinkedList::push_back( Node* newNode){
   if( newNode == nullptr ) 
      return; //if newnode is nothing then just return
   if( tail != nullptr) {//if list is not empty
      newNode->next = nullptr;
      newNode->prev = tail;
      tail->next = newNode;
      tail = newNode;
   }
   else{ //if list is empty
      newNode->next = nullptr;
      newNode->prev = nullptr;
      head = newNode;
      tail = newNode;
   }
   numOfNodes++;
}

//remove a node from the back of the list and return that node, if empty return null
Node* DoubleLinkedList::pop_back(){
   if( head == nullptr)
      return nullptr; //if empty then return nullptr
   if( head == tail ) { //if list only have one 
      Node* returning = tail;
      head = nullptr;
      tail = nullptr;
      numOfNodes--;
      return returning;
   }
   else{
      Node* returning = tail;
      tail = tail->prev;
      tail->next = nullptr;
      returning->prev = nullptr;
      numOfNodes--;
      return returning;
   }

}

//return the very last node from the list, dont make changes to list
Node* DoubleLinkedList::get_last() const{
   return tail;

}

//reutrn the node immediately before currentNode
Node* DoubleLinkedList::get_prev( const Node* currentNode ) const{
   if( head == nullptr )
      return nullptr; //if list empty

   return currentNode->prev;

}

void DoubleLinkedList::swap( Node* node1, Node* node2 ){
   assert( isIn(node1) && isIn(node2));
   if( node1 == node2) //if nodes are same
      return;
   assert( node1 != nullptr && node2 != nullptr);
   if (node1==head){ //if node1 is head
      node1->prev = node2->prev;
      node2->prev->next = node1;
      if (node2->next!= nullptr){ //if node2 not tail
         node2->next->prev = node1;
         node1->next->prev = node2;
         Node* hold = node1->next;
         node1->next = node2->next;
         node2->next = hold;
      }else{ //nod2 tail
         node2->next = node1->next;
         node1->next->prev = node2;
         node1->next = nullptr;
         tail = node1;
      }
      node2->prev = nullptr;
      head = node2;
      return;
   }

   if (node2==head ){ //if node2 head
      node2->prev = node1->prev;
      node1->prev->next = node2;
      if (node1->next!= nullptr){ //if node1 not tail
         node1->next->prev = node2;
         node2->next->prev = node1;
         Node* hold = node2->next;
         node2->next = node1->next;
         node1->next = hold;
      }else{ //if node1 tail
         node1->next = node2->next;
         node2->next->prev = node1;
         node2->next = nullptr;
         tail = node2;
      }
      node1->prev = nullptr;
      head = node1;
      return;
   }

   if (node1==tail){ //if node tail
      node1->next= node2->next;
      node2->next->prev = node1;
      if (node2->prev!= nullptr){ //if node2 not head
         node2->prev->next = node1;
         node1->prev->next = node2;
         Node* hold = node1->prev;
         node1->prev = node2->prev;
         node2->prev = hold;
      }else{ //if node2 head
         node2->prev= node1->prev;
         node1->prev->next = node2;
         node1->prev= nullptr;
         head = node1;
      }
      node2->next = nullptr;
      tail = node2;
      return;
   }
      if (node2==tail){
         node2->next= node1->next;
         node1->next->prev = node2;
         if (node1->prev!= nullptr){
            node1->prev->next = node2;
            node2->prev->next = node1;
            Node* hold = node2->prev;
            node2->prev = node1->prev;
            node1->prev = hold;
         }else{
            node1->prev= node2->prev;
            node2->prev->next = node1;
            node2->prev= nullptr;
            head = node2;
         }
      node1->next = nullptr;
      tail = node1;
      return;
   }
   if( node1->next == node2 && node2->prev == node1 ) {//if nodes next to each other
      
      node2->prev = node1->prev;
      node1->prev->next = node2;
      node1->prev = node2;
      node1->next = node2->next;
      node2->next->prev = node1;
      node2->next = node1;
      return;

   }


   if( node2->next == node1 && node1->prev == node2 ) {//if nodes next to each other      
      node2->next = node1->next;
      node1->next->prev = node2;
      node1->next = node2;
      node1->prev = node2->prev;
      node2->prev->next = node1;
      node2->prev = node1;
      return;

   }

   else{ //normal case
  
      Node* hold1 = node1->next;
      Node* hold2 = node1->prev;
      node1->next = node2->next;
      node2->next->prev = node1;
      node1->prev = node2->prev;
      node2->prev->next = node1;
      node2->next = hold1;
      hold1->prev = node2;
      node2->prev = hold2;
      hold2->next = node2;
      return;
   }

}


const bool DoubleLinkedList::isSorted() const{ //this function depends on Node's < operator
   if( numOfNodes <= 1 ) //if list is empty or only has one item
      return true;

   for( Node* i = head; i->next != nullptr; i = i->next) {
      if( *i > *i->next )
         return false;
   }
   
   return true;
}


void DoubleLinkedList::insertionSort(){ //this function depends on Node's < operator
   if( numOfNodes <= 1 ) 
      return;

   for( Node* start = head; start->next != nullptr; start = start->next ) { //loop to make way along list
      Node* smallest = start;

      for( Node* current = start->next; current != nullptr; current = current->next) { //loop looking down the list
         if( *smallest > *current )
            smallest = current;
      }
      swap( start, smallest );
      start = smallest; //makes sure start is right
            
   }

}


bool DoubleLinkedList::validate() const{
   if( head == nullptr) {
      assert( tail == nullptr );
      assert( numOfNodes == 0 );
      assert( empty() );
   }
   else{
      assert( tail != nullptr );
      assert( numOfNodes != 0);
      assert( !empty() );
   }
   if( tail == nullptr) {
      assert( head == nullptr );
      assert( numOfNodes == 0 );
      assert( empty() );
   }
   else{
      assert( head != nullptr);
      assert( numOfNodes !=0 );
      assert( !empty() );
   }
   if( head != nullptr && tail == head) {
      assert( numOfNodes == 1);
   }

   unsigned int countForward = 0;
   Node* currentNode = head;
   //count forward
   while( currentNode != nullptr ){
      countForward++;
      if( currentNode->next != nullptr) {
         assert( currentNode->next->prev == currentNode );
      }
      currentNode = currentNode->next;
   }
   assert( numOfNodes == countForward);

   //count backwards
   unsigned int countBack = 0;
   currentNode = tail;
   while( currentNode != nullptr ){
      countBack++;
      if( currentNode->prev != nullptr ) {
         assert( currentNode->prev->next == currentNode );
      }
      currentNode = currentNode->prev;
   }
   assert( numOfNodes == countBack );
   return true;
}

//insert newNode immediately after current node
void DoubleLinkedList::insert_after( Node* currentNode, Node* newNode ){
   if( currentNode == nullptr && head == nullptr){
      push_front( newNode );
      return;
   }

   if( currentNode != nullptr && head == nullptr) {
      assert( false ); //cant have currentnode is list empty
   }
   if( currentNode == nullptr && head != nullptr) {
      assert( false ); //something not right
   }
   
   assert( currentNode != nullptr && head != nullptr);

   assert( isIn( currentNode ));

   assert( !isIn( newNode ));

   if( tail != currentNode ){
      newNode->next = currentNode->next;
      currentNode->next = newNode;
      newNode->prev = currentNode;
      newNode->next->prev = newNode;
   }
   else {
      push_back( newNode ); //insert at back
   }
   
   validate();
}
   

//insert newNode immediately before current node
void DoubleLinkedList::insert_before( Node* currentNode, Node* newNode){
   if( currentNode == nullptr && head == nullptr){
      push_front( newNode );
      return;
   }

   if( currentNode != nullptr && head == nullptr) {
      assert( false ); //cant have currentnode is list empty
   }
   if( currentNode == nullptr && head != nullptr) {
      assert( false ); //something not right
   }
   
   assert( currentNode != nullptr && head != nullptr);

   assert( isIn( currentNode ));

   assert( !isIn( newNode ));

   if( head != currentNode ){
      newNode->prev = currentNode->prev;
      currentNode->prev = newNode;
      newNode->next = currentNode;
      newNode->prev->next = newNode;
   }
   else {
      push_front( newNode ); //insert at front
   }
      validate();
  

}

//check if node is in list
const bool DoubleLinkedList::isIn( Node* aNode ) const{
   Node* currentNode = head;

   while( currentNode != nullptr ) {
      if( aNode == currentNode )
         return true;
      currentNode = currentNode->next;
   }
   return false;
}


